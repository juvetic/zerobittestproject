﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeHeroGame
{

    public class UIManager : MonoBehaviour
    {
        public static UIManager instance { get; private set; }
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;

                //Add StatUI GameObjects To The Dictionary With Their Names As The Keys 

                foreach (StatUI UI in _UI)
                {
                    UIDict.Add(UI.name, UI);
                }

                //When the battle avatar changes, the UI on the left side will change

                OnBattleAvatarChangeCallback += UpdateLeftSidedUI;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #region StatUI

        Dictionary<string, StatUI> UIDict = new Dictionary<string, StatUI>();

        [SerializeField]
        StatUI[] _UI;

        //Update the StatUI using name with the new Stat of Avatar Agent
        public void UpdateSpecificUI(string UIname, AvatarAgent avatarAgent)
        {
            if (UIDict.TryGetValue(UIname, out StatUI temp))
            {
                temp.UpdateStatUI(avatarAgent);
            }
        }

        //Update the Current Battle Avatar Stat (I use this when the battle avatar is changed)
        void UpdateLeftSidedUI()
        {
            UpdateSpecificUI("CurrentAvatarInformation", GameManager.instance.GetPlayer().GetBattleAgent());
        }

        //Update the StatUI using name with the new Stat of Avatar Agent
        public void UpdateSpecificHPUI(string UIname, int newHP)
        {
            if (UIDict.TryGetValue(UIname, out StatUI temp))
            {
                temp.UpdatePlayerHPUI(newHP.ToString());
            }
        }

        #endregion

        public delegate void OnBattleAvatarChange();
        public OnBattleAvatarChange OnBattleAvatarChangeCallback;

        int scoreAmount;

        [Tooltip("Text that tells score of the players")]
        [SerializeField]
        Text scoreText;

        [Tooltip("UI that tells players when the battle avatar has 2x damage up")]
        [SerializeField]
        GameObject damageUPUI;

        public void AddScore(int score) 
        {
            scoreAmount += score;
            scoreText.text = scoreAmount.ToString();
        }

        public int GetScore()
        {
            return scoreAmount;
        }

        public void SetActiveDamageUPUI(bool active) 
        {
            damageUPUI.SetActive(active);
        }


    }
}
