﻿using SnakeHeroGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeHeroGame
{
    public class LevelGrid : MonoBehaviour
    {
        private Vector2 Position;

        [Tooltip("leftmost side of screen")]
        [SerializeField]
        float minXPos;

        [Tooltip("bottom of screen")]
        [SerializeField]
        float minYPos;

        [Tooltip("top of screen")]
        [SerializeField]
        float maxXPos;

        [Tooltip("top of screen")]
        [SerializeField]
        float maxYPos;

        [Tooltip("count to when the first object start spawn")]
        [SerializeField]
        float startSpawnTime;

        [Tooltip("count to when the first object start spawn")]
        [SerializeField]
        float spawnRate;

        void Start()
        {
            //Spawn Collidable Agent Repeatedly
            InvokeRepeating("SpawnCollidableAgent", startSpawnTime, spawnRate);

            //stop spawn repeating (I use this when the game is over)
            GameManager.instance.OnGameOverCallback += StopSpawn;
        }

        //Random the position and spawns the collidable object at that position
        public void SpawnCollidableAgent()
        {
            Position = new Vector2(Random.Range(minXPos, maxXPos), Random.Range(minYPos, maxYPos));

            GameManager.instance.GetAvatarManager().SpawnCollidableAgentAtGridPosition(Position);
        }

        void StopSpawn() 
        {
            CancelInvoke();
        }
    }
}