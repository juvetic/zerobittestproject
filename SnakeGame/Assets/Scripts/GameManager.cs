﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeHeroGame
{
    /*/
     * game controller that manages the state of the game
    /*/
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance { get; private set; }
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        [SerializeField]
        PlayerController player;
        public PlayerController GetPlayer()
        {
            return player;
        }
        [SerializeField]
        AvatarManager avatarManager;
        public AvatarManager GetAvatarManager()
        {
            return avatarManager;
        }
        [SerializeField]
        BattleManager battleManager;
        public BattleManager GetBattleManager()
        {
            return battleManager;
        }

        #region Game Over
        public delegate void OnGameOver();
        public OnGameOver OnGameOverCallback;

        bool gameOver = false;

        [SerializeField]
        GameOverUI gameOverUI;

        public bool GetGameOver() 
        {
            return gameOver;
        }

        public void WhenTheGameIsOver() 
        {
            if (OnGameOverCallback != null)
            {
                OnGameOverCallback.Invoke();
            }
            gameOver = true;
            gameOverUI.gameObject.SetActive(true);
        }
        #endregion


    }
}