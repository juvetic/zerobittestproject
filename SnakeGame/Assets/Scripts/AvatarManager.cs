﻿using UnityEngine;

namespace SnakeHeroGame
{
    public class AvatarManager : MonoBehaviour
    {
        [Tooltip("Agents that will be spawned(In this casr, it's either hero or enemy)")]
        [SerializeField]
        CollidableAgent[] collidableAgents;

        [Tooltip("Hero Prefab that will be attached to the line of the player")]
        [SerializeField]
        HeroAttachedAgent heroAttachedAgent;

        //Use Only at the start of the game
        public HeroAttachedAgent GenerateAttachedStartingHero()
        {
            HeroAttachedAgent hero = Instantiate(heroAttachedAgent, new Vector3(0, 0, 0), Quaternion.identity, GameManager.instance.GetPlayer().transform);
            hero.SetupInitialValue();

            return hero;
        }

        //When collide with hero, add it to the line of player's heroes
        public HeroAttachedAgent GenerateAttachedHeroWhenPlayerCollide(CollidableAgent collideAgent)
        {
            HeroAttachedAgent hero = Instantiate(heroAttachedAgent, GameManager.instance.GetPlayer().GetAgentMovingInformationAtIndex(GameManager.instance.GetPlayer().GetAgentMovingInformation().Count - 1).GetPosition(), Quaternion.identity, GameManager.instance.GetPlayer().transform);
            hero.SetupInitialValue(collideAgent.GetHP(), collideAgent.GetAttackValue(), collideAgent.GetDefenseValue(), collideAgent.GetColorType());

            return hero;
        }

        //Use For Changing the leading avatar by copying
        public HeroAttachedAgent CopyAttachedHero(Vector3 startingPosition, HeroAttachedAgent AttachedHeroAgent)
        {
            HeroAttachedAgent hero = Instantiate(heroAttachedAgent, startingPosition, Quaternion.identity, GameManager.instance.GetPlayer().transform);
            hero.SetupInitialValue(AttachedHeroAgent.GetHP(), AttachedHeroAgent.GetAttackValue(), AttachedHeroAgent.GetDefenseValue(), AttachedHeroAgent.GetColorType());

            return hero;
        }

        //Use For Spawning Collidable Agent
        public void SpawnCollidableAgentAtGridPosition(Vector2 Position)
        {
            Instantiate(collidableAgents[UnityEngine.Random.Range(0, collidableAgents.Length)], Position, Quaternion.identity);
        }
    }
}