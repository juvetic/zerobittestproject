﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    [SerializeField]
    Button PlayButton;

    [SerializeField]
    Button ExitButton;

    void Start()
    {
        PlayButton.onClick.AddListener(GoToGameScene);
        ExitButton.onClick.AddListener(Exit);
    }
    void GoToGameScene()
    {
        SceneManager.LoadScene("GameScene");
    }
    void Exit()
    {
        Application.Quit();
    }
}
