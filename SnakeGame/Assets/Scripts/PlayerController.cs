﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace SnakeHeroGame 
{
    //Direction that the agent is facing
    public enum FacingDirection 
    {
        Up,
        Down,
        Right,
        Left
    }

    /*/
      Store Moving Formation For These Purposes 
        -Properly add new hero avatar to the line of existing avatar(s) 
        -Make hero avatar on the list moves like body parts of snakes
    /*/
    public class AgentMovingInformation
    {
        public AgentMovingInformation(Vector2 Position, FacingDirection facingDirection)
        {
            this.Position = Position;
            this.facingDirection = facingDirection;
        }

        Vector2 Position;

        FacingDirection facingDirection;

        public Vector2 GetPosition() { return Position; }

        public FacingDirection GetFacingDirection() { return facingDirection; }
    }
    /*/
     * 
     /*/
    public class PlayerController : MonoBehaviour
    {

        #region Player Movement Attributes

        private Vector2 MoveDirection;
        private Vector2 Position;
        //Keep Track Of When Players Will Move Next
        private float moveTimeCount;

        [Tooltip("Starting direction")]
        [SerializeField]
        Vector2 startMovingDirection;

        [Tooltip("Starting position")]
        [SerializeField]
        Vector2 startPosition;

        [Tooltip("how far players can move / moving time")]
        [SerializeField]
        float translationIndex;

        [Tooltip("how fast players can move")]
        [SerializeField]
        float movingTimeRate;

        [Tooltip("speed up when destroy an enemy")]
        [SerializeField]
        float increaseMovingTimeRate;

        FacingDirection facingDirection;

        #endregion

        #region Hero Controller

        //store heroes to add it to the line
        List<HeroAttachedAgent> heroAttachedAgent = new List<HeroAttachedAgent>();

        //store the information where first hero previously move to make the following hero move on the same position (make lines be like snake game)
        List<AgentMovingInformation> agentMovingInformation = new List<AgentMovingInformation>();

        //store snake body size
        int bodySize;

        #endregion
        void Start()
        {
            Position = startPosition;
            moveTimeCount = movingTimeRate;
            MoveDirection = startMovingDirection;

            //Generate Starting Hero
            AddHeroAttachedAgent(GameManager.instance.GetAvatarManager().GenerateAttachedStartingHero());

            //To Update UI On The Left
            OnAvatarChange();
        }

        void Update()
        {
            if (GameManager.instance.GetBattleManager().GetIsBattleHappening() || GameManager.instance.GetGameOver())
                return;

            HandleInput();
            HandleMovement();
        }

        #region GameOver Function

        //When hero dies, check if the game is over or not
        public void OnBattleAgentDie() 
        {
            if (heroAttachedAgent.Count > 1)
            {
                RemoveHero(0);
                OnAvatarChange();
            }
            else 
            {
                GameManager.instance.WhenTheGameIsOver();
            }
        }

        #endregion

        #region Hero Controller Function

        void ChangeLeadingAvatarToTheLeft()
        {
            AddHeroAttachedAgent(GameManager.instance.GetAvatarManager().CopyAttachedHero(heroAttachedAgent[heroAttachedAgent.Count - 1].transform.position, heroAttachedAgent[0]));
            RemoveHero(0);
            OnAvatarChange();
        }
        void ChangeLeadingAvatarToTheRight()
        {
            InsertHeroAttachedAgentAtIndex(0, GameManager.instance.GetAvatarManager().CopyAttachedHero(heroAttachedAgent[heroAttachedAgent.Count - 1].transform.position, heroAttachedAgent[heroAttachedAgent.Count - 1]));
            RemoveHero(heroAttachedAgent.Count - 1);
            OnAvatarChange();
        }

        //the first leading hero will be the one to fight
        public HeroAttachedAgent GetBattleAgent()
        {
            return heroAttachedAgent[0];
        }
        public AgentMovingInformation GetAgentMovingInformationAtIndex(int index)
        {
            return agentMovingInformation[index];
        }
        public List<AgentMovingInformation> GetAgentMovingInformation()
        {
            return agentMovingInformation;
        }
        public void AddHeroAttachedAgent(HeroAttachedAgent hero)
        {
            bodySize++;
            heroAttachedAgent.Add(hero);
        }
        public void InsertHeroAttachedAgentAtIndex(int index, HeroAttachedAgent hero)
        {
            bodySize++;
            heroAttachedAgent.Insert(index, hero);
        }
        public void RemoveHero(int indexToRemove)
        {
            bodySize--;
            HeroAttachedAgent hero = heroAttachedAgent[indexToRemove];
            heroAttachedAgent.RemoveAt(indexToRemove);
            Destroy(hero.gameObject);
        }

        public void OnAvatarChange()
        {
            if (UIManager.instance.OnBattleAvatarChangeCallback != null)
                UIManager.instance.OnBattleAvatarChangeCallback.Invoke();
        }
        #endregion

        #region Movement Controller Function
        
        private void HandleInput()
        {
            //Move and change direction according to the buttons
            if (Input.GetKeyDown(KeyCode.W))
            {
                ChangeDirection(FacingDirection.Up);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                ChangeDirection(FacingDirection.Down);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                ChangeDirection(FacingDirection.Right);
            }
            else if (Input.GetKeyDown(KeyCode.A))
            {
                ChangeDirection(FacingDirection.Left);
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                ChangeLeadingAvatarToTheLeft();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                ChangeLeadingAvatarToTheRight();
            }
        }

        //Control movement of players and heroes in the line
        private void HandleMovement()
        {
            //add time
            moveTimeCount += Time.deltaTime;
            //start moving when the time reaches the moving time rate
            if (moveTimeCount >= movingTimeRate)
            {
                //Reset the movingTimeCount
                moveTimeCount -= movingTimeRate;

                //insert the movinginformation to make heroes move in line
                agentMovingInformation.Insert(0, new AgentMovingInformation(Position, facingDirection));

                //Update the new position for player
                Position += MoveDirection;

                //if the movinginfo stored is more than or equal the body size + 1, then remove the info on the last index
                if (agentMovingInformation.Count >= bodySize + 1)
                {
                    agentMovingInformation.RemoveAt(agentMovingInformation.Count - 1);
                }

                //update the position of each hero in the line
                int i = 0;
                foreach (HeroAttachedAgent hero in heroAttachedAgent) 
                {
                    hero.transform.position = agentMovingInformation[i].GetPosition();
                    hero.SetAnim(agentMovingInformation[i].GetFacingDirection());
                    i++;
                }

                //assign the updated position
                transform.position = new Vector3(Position.x, Position.y);
            }
        }

        //increase the movingTimeRate when player destroys an enemy, this will make 
        public void increaseMovingTime() 
        {
            movingTimeRate /= increaseMovingTimeRate;
        }

        public void ChangeDirection(FacingDirection moveDirection) 
        {
            switch (moveDirection)
            {
                case FacingDirection.Up:

                    MoveDirection.x = 0;
                    MoveDirection.y = translationIndex;
                    break;
                case FacingDirection.Down:

                    MoveDirection.x = 0;
                    MoveDirection.y = -translationIndex;
                    break;
                case FacingDirection.Right:

                    MoveDirection.x = translationIndex;
                    MoveDirection.y = 0;
                    break;
                case FacingDirection.Left:

                    MoveDirection.x = -translationIndex;
                    MoveDirection.y = 0;
                    break;
                default:
                    break;
            }
            facingDirection = moveDirection;
        }
        #endregion
    }
}

