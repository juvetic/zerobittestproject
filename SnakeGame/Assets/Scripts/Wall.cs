﻿using UnityEngine;

namespace SnakeHeroGame
{
    public class Wall : MonoBehaviour
    {
        [Tooltip("Player Will Move On The Direction When Collide")]
        [SerializeField]
        FacingDirection assignDirectionWhenCollide;

        public void OnTriggerEnter2D(Collider2D col)
        {
            //Check if the player has anymore hero or not, then change the moving direction of player
            if (col.GetComponent<PlayerController>())
            {
                col.GetComponent<PlayerController>().OnBattleAgentDie();

                col.GetComponent<PlayerController>().ChangeDirection(assignDirectionWhenCollide);
            }

        }
    }
}