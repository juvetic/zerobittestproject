﻿using UnityEngine;

namespace SnakeHeroGame
{
    /*/
     * the based agent of all the avatars
    /*/
    public enum ColorType
    {
        Red,
        Green,
        Blue,
    }
    public class AvatarAgent : MonoBehaviour
    {
        //Store information for randomizing stats
        [SerializeField]
        int maxHP;

        [SerializeField]
        int minHP;

        [SerializeField]
        int maxAttackValue;

        [SerializeField]
        int minAttackValue;

        [SerializeField]
        int maxDefenseValue;

        [SerializeField]
        int minDefenseValue;

        protected int HP;

        public void SetHP(int hp)
        {
            HP = hp;
        }
        public int GetHP() 
        {
            return HP;
        }

        protected int AttackValue;

        public int GetAttackValue()
        {
            return AttackValue;
        }

        protected int DefenseValue;

        public int GetDefenseValue()
        {
            return DefenseValue;
        }

        protected ColorType colorType;

        public ColorType GetColorType()
        {
            return colorType;
        }

        [SerializeField]
        protected Renderer rend;

        public Renderer GetRenderer() 
        {
            return rend;
        }

        //Set Up the initial value
        public void SetupInitialValue()
        {
            HP = Random.Range(minHP, maxHP);
            AttackValue = Random.Range(minAttackValue, maxAttackValue);
            DefenseValue = Random.Range(minDefenseValue, maxDefenseValue);
            AssignColor();
        }

        void AssignColor() 
        {
            colorType = (ColorType)Random.Range(0, 3);

            switch (colorType)
            {
                case ColorType.Red:
                    rend.material.color = Color.red;
                    break;
                case ColorType.Green:
                    rend.material.color = Color.green;
                    break;
                case ColorType.Blue:
                    rend.material.color = Color.blue;
                    break;
                default:
                    break;
            }
        }

    }
}