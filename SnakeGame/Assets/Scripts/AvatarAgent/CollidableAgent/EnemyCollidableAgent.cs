﻿using UnityEngine;

namespace SnakeHeroGame
{
    public class EnemyCollidableAgent : CollidableAgent
    {
        //if it collides with player, starts the battle
        public override void OnTriggerEnter2D(Collider2D col)
        {
            if (col.GetComponent<PlayerController>())
            {
                GameManager.instance.GetBattleManager().CommenceBattle(col.GetComponent<PlayerController>().GetBattleAgent(), this);

                GameManager.instance.GetBattleManager().SetBattleIsHappening(true);
            }

        }
    }
}