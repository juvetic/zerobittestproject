﻿using UnityEngine;

namespace SnakeHeroGame
{
    public class CollidableAgent : AvatarAgent
    {
        public virtual void Start() 
        {
            SetupInitialValue();
        }

        public virtual void OnTriggerEnter2D(Collider2D col)
        {
            Destroy(this.gameObject);
        }
    }
}