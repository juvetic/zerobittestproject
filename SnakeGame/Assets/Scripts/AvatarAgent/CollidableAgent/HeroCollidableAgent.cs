﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeHeroGame
{
    public class HeroCollidableAgent : CollidableAgent
    {
        [Tooltip("HeroPrefab To Be Added to player's hero list")]
        [SerializeField]
        HeroAttachedAgent heroAttachedAgent;

        public override void Start()
        {
            base.Start();
        }

        public override void OnTriggerEnter2D(Collider2D col)
        {
            //When collide with player spawn a new hero with the same formation as this and add it to the latest position of player's hero list
            if (col.GetComponent<PlayerController>())
            {
                HeroAttachedAgent hero = GameManager.instance.GetAvatarManager().GenerateAttachedHeroWhenPlayerCollide(this);

                GameManager.instance.GetPlayer().AddHeroAttachedAgent(hero);

                base.OnTriggerEnter2D(col);
            }
            
        }
    }
}