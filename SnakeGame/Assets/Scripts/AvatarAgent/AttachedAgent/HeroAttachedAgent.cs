﻿using UnityEngine;

namespace SnakeHeroGame
{
    /*/
     * hero type that will be added to the line of player
    /*/
    public class HeroAttachedAgent : AvatarAgent
    {
        [SerializeField]
        Animator anim;

        public void SetupInitialValue(int hp, int attackValue, int defenseValue, ColorType colorType) 
        {
            HP = hp;
            AttackValue = attackValue;
            DefenseValue = defenseValue;
            this.colorType = colorType;
            SetColor(colorType);
        }

        public void SetAnim(FacingDirection facingPosition)
        {
            anim.SetInteger("Direction", (int)facingPosition);
        }

        void SetColor(ColorType colorType) 
        {
            switch (colorType)
            {
                case ColorType.Red:
                    rend.material.color = Color.red;
                    break;
                case ColorType.Green:
                    rend.material.color = Color.green;
                    break;
                case ColorType.Blue:
                    rend.material.color = Color.blue;
                    break;
                default:
                    break;
            }
        }
    }
}