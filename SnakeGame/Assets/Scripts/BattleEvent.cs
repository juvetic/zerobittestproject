﻿using UnityEngine;

namespace SnakeHeroGame
{
    public class BattleEvent : MonoBehaviour
    {
        [Tooltip("animator that shows fighting scene and result")]
        [SerializeField]
        Animator battleUIAnimator;

        BattleManager battleManager;

        void Start()
        {
            battleManager = GameManager.instance.GetBattleManager();
        }

        //Player always attacks first
        void OnEnable()
        {
            StartBattle();
        }

        public void StartBattle()
        {
            PlayerAttack();
        }

        /*/when battle ends
        -movingTimeRate will increase, 
        -enemy will be destroyed disregards of its health
        -check hp of the fighting hero, if it's o destroys
        -if player has only 1 hero left and it dies, the game is over
        /*/
        public void EndBattle() 
        {
            GameManager.instance.GetPlayer().increaseMovingTime();

            battleManager.SetBattleIsHappening(false);

            Destroy(battleManager.GetEnemyAvatar().gameObject);

            if (GameManager.instance.GetPlayer().GetBattleAgent().GetHP() <= 0)
            {
                Destroy(GameManager.instance.GetPlayer().GetBattleAgent().gameObject);

                GameManager.instance.GetPlayer().OnBattleAgentDie();
            }
            else 
            {
                UIManager.instance.UpdateSpecificHPUI("CurrentAvatarInformation", GameManager.instance.GetPlayer().GetBattleAgent().GetHP());
            }

            UIManager.instance.SetActiveDamageUPUI(false);
        }

        public void PlayerAttack()
        {
            battleUIAnimator.Play("PlayerAttackAnim");
        }

        public void EnemyAttack()
        {
            battleUIAnimator.Play("EnemyAttackAnim");
        }

        //Use it for animation event to show that the health of the player decreases
        public void UpdatePlayerHP() 
        {
            if (GameManager.instance.GetPlayer().GetBattleAgent())
                UIManager.instance.UpdateSpecificHPUI("PlayerBattleAvatar", GameManager.instance.GetPlayer().GetBattleAgent().GetHP());
        }

        //Use it for animation event to show that the health of the enemy decreases
        public void UpdateEnemyHP()
        {
            if(battleManager.GetEnemyAvatar())
                UIManager.instance.UpdateSpecificHPUI("EnemyBattleAvatar", battleManager.GetEnemyAvatar().GetHP());
        }

        //Use for animation event when player finishes battle, if the enemy doesn't die, it continues to attack
        public void CheckContinueBattle() 
        {

            if (GameManager.instance.GetBattleManager().CheckContinueBattle())
            {
                battleManager.UpdatePlayerDamageReceivedUI(battleManager.BattleDamageCalculation(battleManager.GetEnemyAvatar(), battleManager.GetPlayerAvatar()));

                EnemyAttack();
            }
            else 
            {
                EndBattle();
            }
        }


    }
}