﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace SnakeHeroGame
{
    public class GameOverUI : MonoBehaviour
    {
        [SerializeField]
        Button RetryButton;

        [SerializeField]
        Button ExitButton;

        [SerializeField]
        Text scoreText;

        // Start is called before the first frame update
        void Start()
        {
            RetryButton.onClick.AddListener(Retry);
            ExitButton.onClick.AddListener(Exit);
        }
        private void OnEnable()
        {
            GameOver();
        }

        public void GameOver() 
        {
            if(scoreText)
            scoreText.text = UIManager.instance.GetScore().ToString();
        }

        void Retry() 
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        void Exit() 
        {
            Application.Quit();
        }
    }
}