﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeHeroGame
{
    /*/
     * Control Battle When Hero and enemy fight
    /*/
    public class BattleManager : MonoBehaviour
    {
        //track if the battle is happening or not
        bool isBattleHappening = false;
        public bool GetIsBattleHappening() { return isBattleHappening; }

        public void SetBattleIsHappening(bool battle)
        {
            isBattleHappening = battle;

            battleUI.SetActive(battle);
        }

        #region BattleUI

        [SerializeField]
        GameObject battleUI;
        [SerializeField]
        Text playerDamageReceivedUI;
        [SerializeField]
        Text enemyDamageReceivedUI;

        #endregion

        #region PlayerAndEnemyAvatars

        AvatarAgent playerAvatar;

        AvatarAgent enemyAvatar;
        public AvatarAgent GetPlayerAvatar() { return playerAvatar; }
        public AvatarAgent GetEnemyAvatar() { return enemyAvatar; }

        #endregion


        //Call when hero and enemy fight
        public void CommenceBattle(AvatarAgent _playerAvatar, AvatarAgent _enemyAvatar)
        {
            playerAvatar = _playerAvatar;

            enemyAvatar = _enemyAvatar;

            AssignCharacterToBattleUI();

            UpdateEnemyDamageReceivedUI(BattleDamageCalculation(GetPlayerAvatar(), GetEnemyAvatar()));
        }

        //calculate damage, set the left over hp, and apply the special condition if both have the same color
        public int BattleDamageCalculation(AvatarAgent attacker, AvatarAgent defender) 
        {
            int avatarDamage = attacker.GetAttackValue() - defender.GetDefenseValue();

            if (playerAvatar != null && attacker == playerAvatar) 
            {
                if (attacker.GetColorType() == defender.GetColorType())
                {
                    UIManager.instance.SetActiveDamageUPUI(true);
                    avatarDamage *= 2;
                }
            }

            if (avatarDamage <= 0)
                avatarDamage = 0;

            int leftOverHeart = defender.GetHP() - avatarDamage;

            if(attacker == playerAvatar)
            UIManager.instance.AddScore(defender.GetHP() - Mathf.Abs(leftOverHeart));

            defender.SetHP(leftOverHeart);

            return avatarDamage;
        }

        //Update the UI according to the hero and enemy that are fighting
        void AssignCharacterToBattleUI() 
        {
            if (playerAvatar) 
            {
                UIManager.instance.UpdateSpecificUI("PlayerBattleAvatar", playerAvatar);
            }

            if (enemyAvatar)
            {
                UIManager.instance.UpdateSpecificUI("EnemyBattleAvatar", enemyAvatar);
            }
        }

        //Use it on the anim event when the player is hit
        public void UpdatePlayerDamageReceivedUI(int damage)
        {
            playerDamageReceivedUI.text = damage.ToString();
        }

        //Use it on the anim event when the enemy is hit
        public void UpdateEnemyDamageReceivedUI(int damage)
        {
            enemyDamageReceivedUI.text = damage.ToString();
        }

        //if the enemy doesn't die, continue the battle
        public bool CheckContinueBattle() 
        {
            if (enemyAvatar) 
            {
                return enemyAvatar.GetHP() <= 0 ? false : true;
            }
            return false;
        }


    }
}