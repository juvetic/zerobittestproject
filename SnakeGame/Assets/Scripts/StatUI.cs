﻿using UnityEngine;
using UnityEngine.UI;

namespace SnakeHeroGame
{
    /*/
     * use it for UI that tells status of the avatar
     /*/
    public class StatUI : MonoBehaviour
    {
        public void UpdateStatUI(AvatarAgent _avatar)
        {
            ImageDisplay.color = _avatar.GetRenderer().material.color;
            HPText.text = _avatar.GetHP().ToString();
            attackValueText.text = _avatar.GetAttackValue().ToString();
            defenseValueText.text = _avatar.GetDefenseValue().ToString();
            colorTypeText.text = _avatar.GetColorType().ToString();
        }

        [SerializeField]
        Text HPText;
        public void UpdatePlayerHPUI(string HP)
        {
            HPText.text = HP;
        }

        [SerializeField]
        Text attackValueText;

        [SerializeField]
        Text defenseValueText;

        [SerializeField]
        Text colorTypeText;

        [SerializeField]
        Image ImageDisplay;
    }
}